package ru.ovsyannikov.tm.controller;

import ru.ovsyannikov.tm.entity.Project;
import ru.ovsyannikov.tm.enumerated.Role;
import ru.ovsyannikov.tm.service.ProjectService;
import ru.ovsyannikov.tm.service.ProjectTaskService;
import ru.ovsyannikov.tm.service.UserService;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ProjectController extends AbstractController {

    private final ProjectService projectService;
    private final ProjectTaskService projectTaskService;
    private final UserService userService;

    public ProjectController(ProjectService projectService, ProjectTaskService projectTaskService, UserService userService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
        this.userService = userService;
    }

    public int createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        System.out.println("[OK]");
        System.out.println("PLEASE, ENTER PROJECT DESCRIPTION:");
        final String description = scanner.nextLine();
        System.out.println("[OK]");
//        if (userService.currentUser == null)projectService.create(name, description);
//        else
        System.out.println("PLEASE, ENTER EXECUTOR NAME:");
        final String executorName = scanner.nextLine();
        projectService.create(name, description, userService.currentUser.getId(), userService.currentUser.getLogin(), executorName);
        System.out.println("[OK]");
        return 0;
    }

    public int updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("PLEASE, ENTER PROJECT INDEX:");
        final int index = Integer.parseInt(scanner.nextLine()) -1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return 0;}
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        System.out.println("[OK]");
        System.out.println("PLEASE, ENTER PROJECT DESCRIPTION:");
        final String discription = scanner.nextLine();
        System.out.println("[OK]");
        System.out.println("PLEASE, ENTER EXECUTOR NAME:");
        final String executorName = scanner.nextLine();
        projectService.update(project.getId(), name, discription,executorName);
        System.out.println("[OK]");
        return 0;
    }

    public int setExecutorByIndex() {
        System.out.println("[SET PROJECT EXECUTOR]");
        System.out.println("PLEASE, ENTER PROJECT INDEX:");
        final int index = Integer.parseInt(scanner.nextLine()) -1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return 0;}
        System.out.println("PLEASE, ENTER EXECUTOR NAME:");
        final String executorName = scanner.nextLine();
        projectService.setExecutor(project.getId(),executorName);
        System.out.println("[OK]");
        return 0;
    }

    public int removeProjectByName() {
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        final Project project = projectService.removeByName(name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeProjectByID() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final Long id = Long.parseLong(scanner.nextLine());
        final Project project = projectService.removeById(id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("PLEASE, ENTER PROJECT INDEX:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Project project = projectService.removeByIndex(index);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int clearProject() {
        System.out.println("[CLEAR PROJECT]");
        if(userService.currentUser == null){projectService.clear();}
        else{
            for(Project project: projectService.findAllByUserId(userService.currentUser.getId())){
                projectService.removeById(project.getId());}
        }
        System.out.println("[OK]");
        return 0;
    }

    public void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("CREATOR: " + project.getCreatorName());
        System.out.println("EXECUTOR: " + project.getExecutorName());
        System.out.println("[OK]");
    }

    public int viewProjectByIndex() {
        System.out.println("[ENTER PROJECT INDEX]");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Project project = projectService.findByIndex(index);
        viewProject(project);
        return 0;
    }

    public int listProject() {
        System.out.println("[LIST PROJECT]");
        int index = 1;
        List<Project> projectList;
        if (userService.currentUser == null | userService.currentUser.getRole()== Role.ADMIN) projectList = projectService.findAll();
        else projectList = projectService.findAllByUserId(userService.currentUser.getId());
        Collections.sort(projectList, new Comparator<Project>() {
            public int compare(Project o1, Project o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        for (final Project project : projectList) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName() + " CREATOR: " + project.getCreatorName()
                    + " EXECUTOR: " + project.getCreatorName());
            index++;
        }
        if (index > 1) System.out.println("[OK]");
        else System.out.println("[PROJECTS ARE NOT FOUND]");
        return 0;
    }

}
