package ru.ovsyannikov.tm.controller;

import ru.ovsyannikov.tm.entity.User;
import ru.ovsyannikov.tm.enumerated.Role;
import ru.ovsyannikov.tm.service.UserService;

import java.util.Arrays;
import java.util.List;


public class UserController extends AbstractController {

    private final UserService userService;
    public UserController(UserService userService) {
        this.userService = userService;
    }

    public int createUser() {
        System.out.println("[PLEASE, ENTER LOGIN:]");
        final String login = scanner.nextLine();
        System.out.println("[PLEASE, ENTER password:]");
        final String password = scanner.nextLine();
        System.out.println("[PLEASE, ENTER FIRST NAME:]");
        final String firstName = scanner.nextLine();
        System.out.println("[PLEASE, ENTER LAST NAME:]");
        final String lastName = scanner.nextLine();

        if (userService.createUser(login, password, Role.USER, firstName, lastName) == null) {
            System.out.println("[ERROR! CHOOSE ANOTHER LOGIN]");
        } else {
            System.out.println("[OK]");
        }
        return 0;
    }

    public int updateUser() {
        if (userService.currentUser == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[PLEASE, ENTER PASSWORD:]");
            final String password = scanner.nextLine();
            System.out.println("[PLEASE, ENTER FIRST NAME:]");
            final String firstName = scanner.nextLine();
            System.out.println("[PLEASE, ENTER LAST NAME:]");
            final String lastName = scanner.nextLine();
            userService.updateById(userService.currentUser.getId(), password, firstName, lastName);
            System.out.println("[OK]");
        }
        return 0;
    }

    public int updateUserByLogin() {
        System.out.println("[UPDATE USER]");
        System.out.println("[PLEASE, ENTER LOGIN:]");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[PLEASE, ENTER PASSWORD:]");
            final String password = scanner.nextLine();
            System.out.println("[PLEASE, ENTER FIRST NAME:]");
            final String firstName = scanner.nextLine();
            System.out.println("[PLEASE, ENTER LAST NAME:]");
            final String lastName = scanner.nextLine();
            userService.updateByLogin(login, password, firstName, lastName);
            System.out.println("[OK]");
        }
        return 0;
    }

    public int updateUserById() {
        System.out.println("[UPDATE USER]");
        System.out.println("[PLEASE, ENTER USER ID:]");
        final Long id = Long.parseLong(scanner.nextLine());
        final User user = userService.findById(id);
        if (user == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[PLEASE, ENTER PASSWORD:]");
            final String password = scanner.nextLine();
            System.out.println("[PLEASE, ENTER FIRST NAME:]");
            final String firstName = scanner.nextLine();
            System.out.println("[PLEASE, ENTER LAST NAME:]");
            final String lastName = scanner.nextLine();
            userService.updateById(user.getId(), password, firstName, lastName);
            System.out.println("[OK]");
        }
        return 0;
    }

    public int updatePassword() {
        System.out.println("[UPDATE PASSWORD OF CURRENT USER]");
        if (userService.currentUser == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[PLEASE, ENTER OLD PASSWORD:]");
            final String oldPassword = scanner.nextLine();
            if (userService.checkPassword(userService.currentUser, oldPassword)) {
                System.out.println("[PLEASE, ENTER NEW PASSWORD:]");
                final String newPassword = scanner.nextLine();
                userService.updatePasswordByLogin(userService.currentUser.getLogin(), newPassword);
                System.out.println("[OK]");
            } else {
                System.out.println("[WRONG PASSWORD]");
            }
        }
        return 0;
    }

    public int updatePasswordById() {
        System.out.println("[UPDATE USER]");
        System.out.println("[PLEASE, ENTER USER ID:]");
        final Long id = Long.parseLong(scanner.nextLine());
        final User user = userService.findById(id);
        if (user == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[PLEASE, ENTER PASSWORD:]");
            final String password = scanner.nextLine();
            userService.updatePasswordById(user.getId(), password);
            System.out.println("[OK]");
        }
        return 0;
    }

    public int updatePasswordByLogin() {
        System.out.println("[UPDATE USER]");
        System.out.println("[PLEASE, ENTER LOGIN:]");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[PLEASE, ENTER PASSWORD:]");
            final String password = scanner.nextLine();
            userService.updatePasswordByLogin(login, password);
            System.out.println("[OK]");
        }
        return 0;
    }


    public int clearUser() {
        System.out.println("[CLEAR USER]");
        userService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public int removeUserByLogin() {
        System.out.println("[CLEAR USER BY LOGIN]");
        System.out.println("ENTER LOGIN: ");
        final String login = scanner.nextLine();
        final User user = userService.removeByLogin(login);
        if (user == null)
            System.out.println("[FAIL]");
        else
            System.out.println("[OK]");
        return 0;
    }

    public int removeUserById() {
        System.out.println("[CLEAR USER BY ID]");
        System.out.println("ENTER ID: ");
        final Long id = Long.parseLong(scanner.nextLine());
        final User user = userService.removeById(id);
        if (user == null)
            System.out.println("[FAIL]");
        else
            System.out.println("[OK]");
        return 0;
    }

    public void viewUser(final User user) {
        if (user == null) return;
        System.out.println("[VIEW USER]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("PASSWORD HASH: " + user.getPassword());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("RIGHTS: " + user.getRole().getDisplayName());
        System.out.println("[OK]");
    }

    public void viewUsers(final List<User> users) {
        if (users == null || users.isEmpty()) {
            System.out.println("[USERS ARE NOT FOUND]");
            return;
        }
        int index = 1;
        for (final User user : users) {
            System.out.println(index + ". " + user.getId() + ": " + user.getLogin());
            index++;
        }
        System.out.println("[OK]");
    }

    public int displayUserInfo() {
        viewUser(userService.currentUser);
        return 0;
    }

    public int listUser() {
        System.out.println("[LIST USER]");
        viewUsers(userService.findAll());
        return 0;
    }

    public int viewUserByLogin() {
        System.out.println("ENTER LOGIN: ");
        final User user = userService.findByLogin(scanner.nextLine());
        viewUser(user);
        return 0;
    }

    public int logIn() {
        System.out.println("ENTER LOGIN: ");
        final User user = userService.findByLogin(scanner.nextLine());
        if (user == null) {
            System.out.println("LOGIN IS NOT FOUND IN SYSTEM.");
            return 0;
        }
        System.out.println("ENTER PASSWORD: ");
        if (userService.checkPassword(user, scanner.nextLine())) {
            System.out.println("WELCOME, " + user.getLogin());
            userService.currentUser = user;
        } else {
            System.out.println("FAIL");
        }
        return 0;
    }

    public int checkAuthorisation(final String param) {
        if (!Arrays.asList(Role.ADMIN.getRights()).contains(param)) {
            System.out.println("WRONG OPERATION.");
            return -1;
        }
        if (userService.currentUser == null) {
            System.out.println("PLEASE, LOG IN OR REGISTRY.");
            return -1;
        }
        if (!Arrays.asList(userService.currentUser.getRole().getRights()).contains(param)) {
            System.out.println("NOT ALLOWED.");
            return -1;
        }

        return 0;
    }

        public int logOff(){
            userService.currentUser = null;
            System.out.println("LOG OFF");
            return 0;
        }

    }
