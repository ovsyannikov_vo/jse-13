package ru.ovsyannikov.tm.controller;

import ru.ovsyannikov.tm.entity.Task;
import ru.ovsyannikov.tm.enumerated.Role;
import ru.ovsyannikov.tm.service.ProjectTaskService;
import ru.ovsyannikov.tm.service.TaskService;
import ru.ovsyannikov.tm.service.UserService;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TaskController extends AbstractController {

    private final TaskService taskService;
    private final ProjectTaskService projectTaskService;
    private final UserService userService;

    public TaskController(TaskService taskService, ProjectTaskService projectTaskService, UserService userService) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
        this.userService = userService;
    }

    public int createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("PLEASE, ENTER TASK NAME:");
        final String name = scanner.nextLine();
        System.out.println("[PLEASE, ENTER TASK DESCRIPTION:]");
        final String description = scanner.nextLine();
        /*if (userService.currentUser == null) taskService.create(name, description);
        else */
        System.out.println("PLEASE, ENTER EXECUTOR NAME:");
        final String executorName = scanner.nextLine();
        taskService.create(name, description, userService.currentUser.getId(), userService.currentUser.getLogin(), executorName);
        System.out.println("[OK]");
        return 0;
    }

    public int updateTaskByIndex() {
        System.out.println("[UPDATE TASK]");
        System.out.println("PLEASE, ENTER TASK INDEX:");
        final int index = Integer.parseInt(scanner.nextLine()) -1;
        final Task task = taskService.findByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER TASK NAME:");
        final String name = scanner.nextLine();
        System.out.println("[OK]");
        System.out.println("PLEASE, ENTER TASK DESCRIPTION:");
        final String discription = scanner.nextLine();
        System.out.println("[OK]");
        System.out.println("PLEASE, ENTER TASK EXECUTOR NAME:");
        final String executorName = scanner.nextLine();
        taskService.update(task.getId(), name, discription, executorName);
        System.out.println("[OK]");
        return 0;
    }

    public int setExecutorByIndex() {
        System.out.println("[SET TASK EXECUTOR]");
        System.out.println("PLEASE, ENTER TASK INDEX:");
        final int index = Integer.parseInt(scanner.nextLine()) -1;
        final Task task = taskService.findByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER TASK EXECUTOR NAME:");
        final String executorName = scanner.nextLine();
        taskService.setExecutor(task.getId(), executorName);
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskByName() {
        System.out.println("[REMOVE TASK BY NAME]");
        System.out.println("PLEASE, ENTER TASK NAME:");
        final String name = scanner.nextLine();
        final Task task = taskService.removeByName(name);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeTaskByID() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("PLEASE, ENTER TASK ID:");
        final Long id = scanner.nextLong();
        final Task task = taskService.removeByID(id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int removeTaskByIndex() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("PLEASE, ENTER TASK INDEX:");
        final int index = scanner.nextInt() - 1;
        final Task task = taskService.removeByIndex(index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int clearTask() {
        System.out.println("[CLEAR TASK]");
        if (userService.currentUser == null) {
            taskService.clear();
            projectTaskService.clear();
        } else {
            for (Task task : taskService.findAllByUserId(userService.currentUser.getId())) {
                taskService.removeByID(task.getId());
            }
        }
        System.out.println("[OK]");
        return 0;
    }

    public void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[VIEW TASK]");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("CREATOR: " + task.getCreatorName());
        System.out.println("EXECUTOR: " + task.getExecutorName());
        System.out.println("[OK]");
    }

    public int viewTaskByIndex() {
        System.out.println("[ENTER TASK INDEX]");
        final int index = scanner.nextInt() - 1;
        final Task task = taskService.findByIndex(index);
        viewTask(task);
        return 0;
    }

    public int listTask() {
        System.out.println("[LIST TASK]");
        List<Task> taskList;
        if (userService.currentUser == null | userService.currentUser.getRole()== Role.ADMIN) taskList = taskService.findAll();
        else taskList = taskService.findAllByUserId(userService.currentUser.getId());
        viewTasks(taskList);
        System.out.println("[OK]");
        return 0;
    }

    public void viewTasks(final List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) return;
        int index = 1;
        Collections.sort(tasks, new Comparator<Task>() {
            public int compare(Task o1, Task o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        for (final Task task: tasks) {
            System.out.println(index + ". " + task.getId() + ": " + task.getName() + " CREATOR: " + task.getCreatorName()
                    + " EXECUTOR: " + task.getCreatorName());
            index++;
        }
    }

    public int listTaskByProjectId() {
        System.out.println("[LIST TASKS BY PROJECT ID]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final Long projectId = Long.parseLong(scanner.nextLine());
        final List<Task> tasks = taskService.findAllByProjectId(projectId);
        viewTasks(tasks);
        System.out.println("[OK]");
        return 0;
    }

    public int addTaskToProjectByIds() {
        System.out.println("[ADD TASK TO PROJECT BY IDS]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final Long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("PLEASE, ENTER TASK ID:");
        final Long taskId = Long.parseLong(scanner.nextLine());
        projectTaskService.addTaskToProject(projectId,taskId);
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskFromProjectByIds() {
        System.out.println("[REMOVE TASK FROM PROJECT BY IDS]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final Long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("PLEASE, ENTER TASK ID:");
        final Long taskId = Long.parseLong(scanner.nextLine());
        projectTaskService.removeTaskFromProject(projectId, taskId);
        System.out.println("[OK]");
        return 0;
    }


}
