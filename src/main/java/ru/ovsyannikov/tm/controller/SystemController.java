package ru.ovsyannikov.tm.controller;

import java.util.ArrayDeque;
import java.util.Deque;

public class SystemController extends AbstractController {

    public Deque<String> history = new ArrayDeque<>();
    public int historyMaxSize = 10;

    public int displayHistory(){
        if (history == null || history.isEmpty()) {
            System.out.println("[HISTORY IS EMPTY]");
            return -1;
        }
        int index = 1;
        for (final String command : history) {
            System.out.println(index + ". " + command);
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    public void addCommandToHistory(String command) {
        if (command == null) return;
        history.add(command);
        if (history.size() > historyMaxSize) {
            history.remove();
        }
    }

        public int displayExit() {
        System.out.println("Terminate program...");
        return 0;
    }

    public int displayError() {
        System.out.println("Error! Unknown program argument...");
        return -1;
    }

    public void displayWelcome() {
        System.out.println("** Welcome to Task-Manager **");
    }

    public int displayAbout() {
        System.out.println("Ovsyannikov Vladislav");
        System.out.println("vldslv.ovsyannikov@gmail.com");
        return 0;
    }

    public int displayVersion() {
        System.out.println("1.0.8");
        return 0;
    }

    public int displayHelp() {
        System.out.println("registry - Create new account.");
        System.out.println("log-in - Log in.");
        System.out.println("log-off - Log off.");
        System.out.println();
        System.out.println("user-create - Create user.");
        System.out.println("user-clear - Clear list of users.");
        System.out.println("user-list - Display list of users.");
        System.out.println("user-view-by-login - Display user by login.");
        System.out.println("user-remove-by-login - Remove user by login.");
        System.out.println("user-update-by-login - Update user by login.");
        System.out.println("user-info - User information.");
        System.out.println("user-update - Update user information.");
        System.out.println("user-update-password - Update current user password.");
        System.out.println("user-password-update-by-id - Update user password by id (admin)");
        System.out.println();
        System.out.println("version - Display application version");
        System.out.println("about - Display developer info");
        System.out.println("help - Display list of commands");
        System.out.println("exit - Terminate console application");
        System.out.println();
        System.out.println("project-list - Display list of projects");
        System.out.println("project-view - Display project by index");
        System.out.println("project-create - Create new project by name");
        System.out.println("project-clear - Delete all projects");
        System.out.println("project-remove-by-name - Delete  project by name");
        System.out.println("project-remove-by-id - Delete project by id");
        System.out.println("project-remove-by-index - Delete project by index");
        System.out.println("project-update-by-index - Update project by index");
        System.out.println("project-set-executor-by-index - Set project executor by project index");
        System.out.println();
        System.out.println("task-list - Display list of tasks");
        System.out.println("task-view - Display task by index");
        System.out.println("task-create- Create new task by name");
        System.out.println("task-clear - Delete all tasks");
        System.out.println("task-remove-by-name - Delete  task by name");
        System.out.println("task-remove-by-id - Delete task by id");
        System.out.println("task-remove-by-index - Delete task by index");
        System.out.println("task-update-by-index - Update task by index");
        System.out.println("tasks-list-by-project-id - Display list of task from project by project_id");
        System.out.println("task-add-to-project-by-ids - Add task to project");
        System.out.println( "task-remove-from-project-by-ids - Remove task from project");
        System.out.println("task-set-executor-by-index - Set task executor by task index");
        return 0;
    }

}
