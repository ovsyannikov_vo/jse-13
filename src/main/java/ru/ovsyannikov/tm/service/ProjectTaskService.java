package ru.ovsyannikov.tm.service;

import ru.ovsyannikov.tm.entity.Project;
import ru.ovsyannikov.tm.entity.Task;
import ru.ovsyannikov.tm.repository.ProjectRepository;
import ru.ovsyannikov.tm.repository.TaskRepository;

import java.util.Collections;
import java.util.List;

public class ProjectTaskService {
    private final ProjectRepository projectRepository;
    private final TaskRepository taskRepository;

    public ProjectTaskService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public List<Task> findAllByProjectId(final Long projectId) {
        if (projectId == null) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }

    public Task removeTaskFromProject(final Long projectId, final Long taskId) {
        final Task task = taskRepository.findByProjectIdAndId(projectId, taskId);
        if (projectId == null) return null;
        //final Task task = taskRepository.findByID(taskId);
        if (taskId == null) return null;
        task.setProjectId(null);
        return task;
    }

    public Task addTaskToProject(final Long projectId, final Long taskId) {
        final Project project = projectRepository.findByID(projectId);
        if (projectId == null) return null;
        final Task task = taskRepository.findByID(taskId);
        if (taskId == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    public void clear() {
        projectRepository.clear();
        taskRepository.clear();
    }

}
