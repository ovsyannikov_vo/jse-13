package ru.ovsyannikov.tm.enumerated;

import static ru.ovsyannikov.tm.constant.TerminalConst.*;

public enum Role {
    USER("User", USER_RIGHTS),
    ADMIN("Administrator", ADMIN_RIGHTS),
    GUEST("Guest", GUEST_RIGHTS);

    private final String displayName;

    private final String[] rights;

    Role(String displayName, String[] rights) {
        this.displayName = displayName;
        this.rights = rights;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String[] getRights() {
        return rights;
    }

    @Override
    public String toString() {
        return displayName;
    }

}
